npm install forever -g
npm install webpack -g

npm install will install both "dependencies" and "devDependencies"
npm install --production will only install "dependencies"
npm install --dev will only install "devDependencies"

scp -r directory root@ip:"directory"

webpack server-dev.js

node server.js
