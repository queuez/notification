import express from 'express';
let app = express(); // the main app

import cors from 'cors';

let corsOptions = {
  origin: '*'
}

//app.use(cors(corsOptions));//set all

let youtube = []

let hasIdYoutubeURL = (url) => {
    var regexp = /https:\/\/www\.youtube\.com\/watch\?v=[_\-a-zA-Z0-9]{0,11}/;
    var matches_array = url.match(regexp);
    if(matches_array !== null && matches_array.length > 0) {
        return matches_array[0].split('?v=')[1];
    }
    return null
}

import http from 'http';
let server = http.Server(app);

import socketIO from 'socket.io';
let io = socketIO.listen(server);

io.sockets.on('connection', function(socket){

    socket.on('updateQueue', function(request){
        socket.broadcast.emit('updateQueue', {
            idex: request.idex
        });
    });

    socket.on('updateMember', function(request){
        io.sockets.emit('updateMember', {
            idex: request.idex
        });
    });

    socket.on('updateMessage', function(request){
        io.sockets.emit('updateMessage', {
            idex: request.idex,
            message: request.message,
            dateTime: (new Date()).toString()
        });
        let idYoutube = hasIdYoutubeURL(request.message)
        if(idYoutube !== null) {
            youtube.push({
                name: request.name,
                id: idYoutube
            })
            io.sockets.emit('updateYoutube', {
            });
            io.sockets.emit('listYoutube', {
                listYoutube: youtube
            });
        }
    });

    socket.on('youtube', function(request){
        if(youtube.length > 0) {
            let idYoutube =  youtube[0];
            youtube = youtube.splice(1);
            socket.emit('youtube', {
                idYoutube: idYoutube,
                count: youtube.length
            });
            socket.emit('listYoutube', {
                listYoutube: youtube
            });
        }
    });

    socket.on('changeYoutube', function(request){
        if(youtube.length > 1) {
            let idex = request.idex
            let status = request.status
            if(status === 'up') {
                let mem1 = youtube[idex]
                let mem2 = youtube[idex-1]
                youtube[idex] = mem2
                youtube[idex-1] = mem1
            } else {
                let mem1 = youtube[idex]
                let mem2 = youtube[idex+1]
                youtube[idex] = mem2
                youtube[idex+1] = mem1
            }
            socket.emit('listYoutube', {
                listYoutube: youtube
            });
        }
    });

});

server.listen(9002, function(){
    console.log('listening on *:9002');
});
